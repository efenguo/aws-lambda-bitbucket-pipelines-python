# Deploy to AWS Lambda
An example script and configuration for deploying a new version to an existing [AWS Lambda](https://aws.amazon.com/lambda/) function with Bitbucket Pipelines.  A small Python function is included to use a demo for trying out the included code and configuration.

## How To Use It
* Optional:  Create an [AWS Lambda function](http://docs.aws.amazon.com/lambda/latest/dg/welcome.html) if you do not already have an existing function you want to update
* Add the required Environment Variables below
* Copy `lambda_deploy.py` to your project
* Copy `bitbucket-pipelines.yml` to your project
    * Or use your own, just be sure to include all steps in the sample yml file
* Copy `sample_lambda_function.py` to your project if you would like to try this using a sample function

## Required Permissions in AWS
It is recommended you [create](http://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html) a separate user account used for this deploy process.  This user should be associated with a group that has the `AWSLambdaFullAccess` [AWS managed policy](http://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_managed-vs-inline.html) attached for the required permissions to execute a new deployment to AWS Lambda.

Note that the above permissions are more than what is required in a real scenario. For any real use, you should limit the access to just the AWS resources in your context.

## Required Environment Variables
* `AWS_SECRET_ACCESS_KEY`:  Secret key for a user with the required permissions.
* `AWS_ACCESS_KEY_ID`:  Access key for a user with the required permissions.
* `AWS_DEFAULT_REGION`:  Region where the target AWS Lambda function is.
* `AWS_LAMBDA_FUNCTION_NAME`:  Name of the target AWS Lambda function.

# License
Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at

    http://aws.amazon.com/apache2.0/

or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

Note: Other license terms may apply to certain, identified software files contained within or distributed with the accompanying software if such terms are included in the directory containing the accompanying software. Such other license terms will then apply in lieu of the terms of the software license above.
